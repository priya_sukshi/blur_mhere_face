# load_model_sample.py
import tensorflow 
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image

import numpy as np
import os
import argparse
import csv

ap=argparse.ArgumentParser()
ap.add_argument('-i','--image',type=str,default='',help='path')
args=vars(ap.parse_args())

image_path=args['image']



def load_image(img_path, show=False):
    print(type(img_path))

    img = image.load_img(img_path, target_size=(224, 224))
    img_tensor = image.img_to_array(img)                    # (height, width, channels)
    img_tensor = np.expand_dims(img_tensor, axis=0)         # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
    img_tensor /= 255.                                      # imshow expects values in the range [0, 1]

    if show:
        plt.imshow(img_tensor[0])                           
        plt.axis('off')
        plt.show()
    return img_tensor


model = load_model("gender_17f_ep15.h5")
images=os.listdir(image_path)
for each_image in images:
    print(str(each_image))
    img_path = os.path.join(image_path,each_image)
    new_image = load_image(img_path)
    pred = model.predict(new_image)
    with open("man.csv",'a',newline='') as csvfile:
        fieldnames=['image_name','pred']
        writer=csv.DictWriter(csvfile,fieldnames)
        writer.writerow({'image_name':each_image,'pred':pred})
